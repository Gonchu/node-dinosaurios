import mongoose from 'mongoose';

// Imporatmos el modelo Pet en este nuevo archivo.
import { Dino } from '../models/Dino.js'

const dinosaurioList = [
    {
        name:'Tyrannosaurus rex',
        diet:'Carnívoro',
        period:'Cretácico Superior',
        location:'Usa y Canadá',
        age: 65 ,
    },
    {
        name:'Allosaurus',
        diet:'Carnívoro',
        period:'Jurásico Superior',
        location:'Usa y Tanzania',
        age: 140 ,
    },
    {
        name:'Archaeopteryx',
        diet:'Carnívoro',
        period:'Jurásico Superior',
        location:'Alemania',
        age: 150 ,
    },
    {
        name:'Carcharodontosaurus',
        diet:'Carnívoro',
        period:'Cretácico Superior',
        location:'África',
        age: 113,
    },
    {
        name:'Carnotaurus',
        diet:'Carnívoro',
        period:'Cretácico Superior',
        location:'Argentina',
        age: 70,
    },
    {
        name:'Giganotosaurus',
        diet:'Carnívoro',
        period:'Cretácico Inferior',
        location:'Argentina',
        age: 110,
    },
    {
        name:'Carcharodon megalodón',
        diet:'Carnívoro',
        period:'Cenozoico',
        location:'América, Australia, Asia y las Islas Canarias',
        age: 19,
    },
    {
        name:'Microraptor',
        diet:'Carnívoro',
        period:'Cratácico Inferior',
        location:'China',
        age: 120,
    },
    {
        name:'Spinosaurus',
        diet:'Carnívoro',
        period:'Cretácico',
        location:'Niger, Egipto, Libia y Marruecos',
        age: 70,
    },
    {
        name:'Triceratops',
        diet:'Herbívoro',
        period:'Cretácico Superior',
        location:'Usa',
        age: 70,
    },
    {
        name:'Diplodocus',
        diet:'Herbívoro',
        period:'Jurásico Superior',
        location:'Usa',
        age: 155,
    },
    {
        name:'Bactrosaurus ',
        diet:'Herbívoro',
        period:'Cretácico Superior',
        location:'China',
        age: 84,
    },
    {
        name:'Ankylosaurus ',
        diet:'Herbívoro',
        period:'Cretácico Superior',
        location:'Usa y Canadá',
        age: 74,
    },
];


const dinoDocuments = dinosaurioList.map(dino => new Dino(dino));

// En este caso, nos conectaremos de nuevo a nuestra base de datos
// pero nos desconectaremos tras insertar los documentos
mongoose
.connect('mongodb://localhost:27017/Dinosaurios', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
})
.then(async () => {
		// Utilizando Dino.find() obtendremos un array con todos los dinosaurios de la db
    const allDinos = await Dino.find();
		
		// Si existen dinosaurios previamente, dropearemos la colección
    if (allDinos.length) {
      await Dino.collection.drop(); //La función drop borra la colección
    }
})
.catch((err) => console.log(`Error deleting data: ${err}`))
.then(async () => {
		// Una vez vaciada la db de los dinosaurios, usaremos el array dinoDocuments
		// para llenar nuestra base de datos con todas los dinosaurios.
		await Dino.insertMany(dinoDocuments);
	})
.catch((err) => console.log(`Error creating data: ${err}`))
	// Por último nos desconectaremos de la DB.
.finally(() => {
    mongoose.disconnect();
    console.log('OK!');
});