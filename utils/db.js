import mongoose from 'mongoose';
import dotenv from 'dotenv';
dotenv.config();

// URL local de nuestra base de datos en mongoose y su nombre Dinosaurios
const DB_URL = process.env.DB_URL;

// Función que conecta nuestro servidor a la base de datos de MongoDB mediante mongoose
const connection = mongoose.connect(DB_URL, {
useNewUrlParser: true,
useUnifiedTopology: true,
});

export { DB_URL, connection };
