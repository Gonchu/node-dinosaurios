import express from 'express';
import { upload, uploadToCloudinary } from '../middlewares/file.middleware.js';

import { Dino } from '../models/Dino.js';

const router = express.Router();

router.get('/', (req, res) => {
console.log(req.user);

    return Dino.find()
    .then(dino => {
    return res.status(200).json(dino);// Si encontramos los personajes, los devolveremos al usuario
    })
    .catch(err => {
    return res.status(500).json(err);// Si hay un error, enviaremos por ahora una respuesta de error.
    });
});

router.post('/', [upload.single('picture'),uploadToCloudinary], async (req, res, next) => {
    try {
		const picture = req.file_url || null;
      // Crearemos una instancia de dino con los datos enviados
    const newDino = new Dino({
        name: req.body.name,
        diet: req.body.diet,
        period: req.body.period,
        location: req.body.location,
        age: req.body.age,
		picture: picture
    });

      // Guardamos el dino en la DB
    const createdDino = await newDino.save();
    return res.status(201).json(createdDino);
    } catch (error) {
          // Lanzamos la función next con el error para que lo gestione Express
    next(error);
    }
});


router.get('/:id', async (req, res) => {
	const { id } = req.params;
	try {
		const dino = await Dino.findById(id);
		if (dino) {
			return res.status(200).json(dino);
		} else {
			return res.status(404).json(`No dino found by this id: ${id}`);//error 400cuando el usuario hace algo mal
		}
	} catch (err) {
		return res.status(500).json(err);//error 500 cuando hay un error interno, error de servidor
	}
});

router.put('/:id', async (req, res, next) => {
    try {
        const { id } = req.params; //Recuperamos el id de la url
        const dino = new Dino(req.body); //instanciamos un nuevo Character con la información del body
        dino._id = id; //añadimos la propiedad _id al personaje creado
        await Dino.findByIdAndUpdate(id , dino);
        return res.status(200).json(dino);//Este personaje que devolvemos es el anterior a su modificación
    } catch (error) {
        return next(error);
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        const {id} = req.params;
        // No será necesaria asignar el resultado a una variable ya que vamos a eliminarlo
        await Dino.findByIdAndDelete(id);
        return res.status(200).json('Dino deleted!');
    } catch (error) {
        return next(error);
    }
});

router.get('/name/:name', async (req, res) => {
	const {name} = req.params;

	try {
		const dinoByName = await Dino.find({ name: name });
		if(dinoByName.length > 0){
			return res.status(200).json(dinoByName);
		} else {
			return res.status(404).json(`No dino found by this name: ${name}`);
		}
		
	} catch (err) {
		return res.status(500).json(err);
	}
});

router.get('/diet/:diet', async (req, res) => {
	const {diet} = req.params;

	try {
		const dinoByDiet = await Dino.find({ diet: diet });
		if(dinoByDiet.length > 0){
			return res.status(200).json(dinoByDiet);
		} else {
			return res.status(404).json(`No dino found by this diet: ${diet}`);
		}
		
	} catch (err) {
		return res.status(500).json(err);
	}
});

router.get('/period/:period', async (req, res) => {
	const {period} = req.params;

	try {
		const dinoByPeriod = await Dino.find({ period: period });
		if(dinoByPeriod.length > 0){
			return res.status(200).json(dinoByPeriod);
		} else {
			return res.status(404).json(`No dino found by this period: ${period}`);
		}
		
	} catch (err) {
		return res.status(500).json(err);
	}
});

router.get('/location/:location', async (req, res) => {
	const {location} = req.params;

	try {
		const dinoByLocation = await Dino.find({ location: location });
		if(dinoByLocation.length > 0){
			return res.status(200).json(dinoByLocation);
		} else {
			return res.status(404).json(`No dino found by this location: ${location}`);
		}
		
	} catch (err) {
		return res.status(500).json(err);
	}
});


router.get('/age/:age', async (req, res) => {
	const {age} = req.params;

	try {
		const dinoByAge = await Dino.find({ age: { $gt:age } });
		return res.status(200).json(dinoByAge);
	} catch (err) {
		return res.status(500).json(err);
	}
});

// router.get('/age/:age', async (req, res) => {
// 	const {age} = req.params;

// 	try {
// 		const dinoByAge = await Dino.find({ age: age });
// 		if(dinoByAge.length > 0){
// 			return res.status(200).json(dinoByAge);
// 		} else {
// 			return res.status(404).json(`No dino found by this age: ${age}`);
// 		}
		
// 	} catch (err) {
// 		return res.status(500).json(err);
// 	}
// });




// router.get('/age/:age', async (req, res) => {
//     const { minAge, maxAge } = req.query;

//     const dinoByAge = {
//     ...(minAge && { $gt: minAge }),
//     ...(maxAge && { $lt: maxAge }),
//     };

//     try {
//     const filters = (minAge || maxAge) ? { age: dinoByAge } : {};
//     const results = await Dino.find(filters);
//     return res.status(200).json(results);
//     } catch (err) {
//     return res.status(500).json(err);
//     }
// });


export { router as dinoRoutes }