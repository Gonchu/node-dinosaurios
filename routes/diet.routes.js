import express from 'express';
import { Diet } from '../models/Diet.js';
const router = express.Router();

router.get('/carnivoro', async (req, res, next) => {
    try {
        const diet = await Diet.find().populate('dino');
        return res.status(200).json(diet)
    } catch (error) {
        return next(error)
    }
});


router.post('/', async (req, res, next) => {
    try {
        const newDiet = new Diet({
            name: req.body.name,
            diet: req.body.diet,
            location: req.body.location,
            dino: []
        });
        const createdDiet = await newDiet.save();
        return res.status(201).json(createdDiet);
    } catch (error) {
        next(error);
    }
});

router.put('/add-dino-carnivoro', async (req, res, next) => {
    try {
        const { dietId } = req.body;
        const { dinoId } = req.body;
        const updatedDiet = await Diet.findByIdAndUpdate(
            dietId,
            { $push: { dino: dinoId } },
            { new: true }
        );
        return res.status(200).json(updatedDiet);
    } catch (error) {
        return next(error);
    }
});

router.put('/add-dino-herbivoro', async (req, res, next) => {
    try {
        const { dietId } = req.body;
        const { dinoId } = req.body;
        const updatedDiet = await Diet.findByIdAndUpdate(
            dietId,
            { $push: { dino: dinoId } },
            { new: true }
        );
        return res.status(200).json(updatedDiet);
    } catch (error) {
        return next(error);
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        const {id} = req.params;
        // No será necesaria asignar el resultado a una variable ya que vamos a eliminarlo
        await Diet.findByIdAndDelete(id);
        return res.status(200).json('Diet deleted!');
    } catch (error) {
        return next(error);
    }
});

export { router as dietRoutes }