import express from 'express';
import dotenv from 'dotenv';
dotenv.config();
import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

import { DB_URL } from './utils/db.js';

import session from 'express-session';
import MongoStore from 'connect-mongo';

import passport from 'passport';
import { isAuth } from './authentication/passport.js';

import { Dino } from './models/Dino.js';
import { userRoutes } from './routes/user.routes.js';
import { dinoRoutes } from './routes/dino.routes.js';
import { dietRoutes } from './routes/diet.routes.js';




// SERVER
// const PORT = 3000;
const PORT = process.env.PORT;
// const PORT = process.env.PORT;
const server = express();

const router = express.Router();

router.get('/', (req, res) => {
res.send('Hello');
});



// middlewares
server.use(express.json());
server.use(express.urlencoded({ extended: true }));
server.use(express.static(path.join(__dirname, 'public')));

// passport and sesion
server.use(
	session({
	  secret: process.env.SESSION_SECRET, // ¡Este secreto tendremos que cambiarlo en producción!
	  resave: false, // Solo guardará la sesión si hay cambios en ella.
	  saveUninitialized: false, // Lo usaremos como false debido a que gestionamos nuestra sesión con Passport
	cookie: {
		maxAge: 3600000 // Milisegundos de duración de nuestra cookie, en este caso será una hora.
	},
	store: MongoStore.create({
		mongoUrl: DB_URL,
	})
	})
);

server.use(passport.initialize());
server.use(passport.session());





//rutas
server.use('/', router);
server.use('/users', userRoutes);
server.use('/dino', [isAuth], dinoRoutes);
server.use('/diet', dietRoutes);

// control error

server.use('*', (req, res, next) => {
	const error = new Error('Route not found'); 
	error.status = 404;
	next(error); // Lanzamos la función next() con un error
});

server.use((err, req, res, next) => {
	return res.status(err.status || 500).json(err.message || 'Unexpected error');
});



server.listen(PORT, () => {
console.log(`Server running in http://localhost:${PORT}`);
});
