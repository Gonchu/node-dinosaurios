import mongoose from 'mongoose';

const Schema = mongoose.Schema;

// Creamos el esquema de dinos
const dinoSchema = new Schema(
  {
    name: { type: String, required: true },
    diet: { type: String, required: true },
    period: { type: String,},//La propiedad required hace que el campo sea obligatorio
    location: { type: String,},
    age: { type: Number },
    picture: { type: String },
  },
  {
    // Esta propiedad servirá para guardar las fechas de creación y actualización de los documentos
    timestamps: true,
  }
);

// Creamos y exportamos el modelo Dino
const Dino = mongoose.model('Dino', dinoSchema);
export { Dino }