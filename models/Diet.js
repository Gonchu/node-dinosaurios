import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const dietSchema = new Schema(
  {
    name: { type: String, required: true },
    location: { type: String, required: true },
		// Tipo mongoose Id y referencia al modelo Dino
    dino: [{ type: mongoose.Types.ObjectId, ref: 'Dino' }],
  },
  {
    timestamps: true,
  }
);

const Diet = mongoose.model('Diet', dietSchema);
export { Diet }